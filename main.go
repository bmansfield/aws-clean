package main

import (
	"flag"
	"github.com/byronmansfield/aws/ami"
	"github.com/byronmansfield/aws/ebs"
	"os"
)

func main() {
	ebsFlags := flag.NewFlagSet("ebs", flag.ContinueOnError)
	amiFlags := flag.NewFlagSet("ami", flag.ContinueOnError)

	switch os.Args[1] {
	case "ebs":
		if err := ebsFlags.Parse(os.Args[2:]); err == nil {
			ebs.Print(os.Args[2:])
		}
	case "ami":
		if err := amiFlags.Parse(os.Args[2:]); err == nil {
			ami.Print()
		}
	}
}
