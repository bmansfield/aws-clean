// ebs-clean is a command line tool for deleting aws ebs snapshots with
// features like control over region, how old to keep and delete,
// dry run and more. Requires aws account env var to be exposed
package ebs

import (
	"bytes"
	"errors"
	"flag"
	"fmt"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/ec2"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strconv"
	"strings"
	"time"
)

const AppVersion = "Demandbase EBS Clean Tool Version: 1.0.0-beta"

var regionPtr string
var olderThanPtr string
var dryRunPtr bool
var helpPtr bool
var versionPtr bool
var verbosePtr bool
var printAllPtr bool
var awsAccountId string
var now time.Time
var svc *ec2.EC2

type SlackMessage struct {
	channel  string `json:"channel,omitempty"`
	username string `json:"username,omitempty"`
	text     string `json:"text,omitempty"`
}

type EBS struct {
	Description string
	SnapshotId  string
	StartTime   time.Time
}

type snapShotSlice []EBS

// interfaces for snapShotSlice
// these are neccissary for sort algorithm
//
// length
func (s snapShotSlice) Len() int {
	return len(s)
}

// check if less
func (s snapShotSlice) Less(i, j int) bool {
	return s[i].StartTime.Before(s[j].StartTime)
}

// swap
func (s snapShotSlice) Swap(i, j int) {
	s[i], s[j] = s[j], s[i]
}

// initializer
func init() {
	flag.BoolVar(&helpPtr, "help", false, "EBS Clean Help: outputs tools options")
	flag.BoolVar(&versionPtr, "version", false, "Display EBS Clean Version information")
	flag.StringVar(&regionPtr, "region", "us-west-1", "Which AWS Region")
	flag.StringVar(&olderThanPtr, "older-than", "120d", "Specify how far back you want to keep (Can handle XXh, XXd, or XXm)")
	flag.BoolVar(&dryRunPtr, "dry-run", false, "dry run (default \"false\")")
	flag.BoolVar(&verbosePtr, "v", false, "Use verbose output (default \"false\")")
	flag.BoolVar(&printAllPtr, "print-all", false, "Prints all the ebs snapshots (default \"false\")")

	flag.Usage = Usage
	flag.Parse()

	awsAccountId = os.Getenv("AWS_ACCT_ID")
	now = time.Now()
}

// entry point function
func main() {

	// Parse Flags:
	// check if any flags where passed
	if flag.NFlag() == 0 {
		flag.Usage()
		os.Exit(1)
	} else if versionPtr {
		Version()
		os.Exit(1)
	} else if helpPtr {
		Usage()
		os.Exit(1)
	} else {
		Clean()
	}
}

func Print(args []string) {
	fmt.Println("print ebs", args)
}

// Begin the ebs clean process
func Clean() {
	// create aws service
	svc = ec2.New(session.New(), &aws.Config{
		Region: &regionPtr,
	})

	// get all snapshots sorted
	allSnapShots := GetAllSnapshots(awsAccountId)

	// split apart at year from now
	yearOld, yearYoung := YearSplit(allSnapShots)

	// print all year old
	if verbosePtr {
		fmt.Println()
		for _, snap := range yearOld {
			fmt.Printf("%s %s Is over a year old\n", snap.SnapshotId, snap.StartTime)
		}
		fmt.Println()
	}

	// print all less than a year
	if verbosePtr {
		for _, snap := range yearYoung {
			fmt.Printf("%s %s Is not a year old\n", snap.SnapshotId, snap.StartTime)
		}
		fmt.Println()
	}

	// make group to delete from younger group that is older than older-than flag
	olderThan := OlderThan(olderThanPtr, yearYoung)

	// remove first of month snapshots from older than group which is to be deleted
	olderThanWithoutFirstOfMonth := RemoveFirstOfMonth(olderThan)

	// print all older than the older-than flag without the first of month
	if verbosePtr {
		fmt.Println()
		for _, snap := range olderThanWithoutFirstOfMonth {
			fmt.Printf("%s %s Is older than the flag and not first of month\n", snap.SnapshotId, snap.StartTime)
		}
	}

	// concatinate older than without first of month to the year old group
	toBeDeleted := ConcatSlices(olderThanWithoutFirstOfMonth, yearOld)

	// print all older than the older-than flag without the first of month
	if verbosePtr {
		for _, snap := range toBeDeleted {
			fmt.Printf("%s %s Is in deleted list\n", snap.SnapshotId, snap.StartTime)
		}
	}

	// print totals
	fmt.Println()

	msg := "Totals for region "
	msg += string(regionPtr)
	msg += "\n"
	msg += "Total number of snapshots: "
	msg += strconv.Itoa(len(allSnapShots))
	msg += "\n"

	if verbosePtr {
		fmt.Println("older than without fist of month list length:", len(olderThanWithoutFirstOfMonth))
		fmt.Println("year old list length:", len(yearOld))
		fmt.Println("concat list length should be:", (len(yearOld) + len(olderThanWithoutFirstOfMonth)))
	}

	if dryRunPtr {
		msg += "number of snapshots that would get deleted: "
		msg += strconv.Itoa(len(toBeDeleted))
		msg += "\n"
	} else {
		msg += "number of snapshots deleted: "
		msg += strconv.Itoa(len(toBeDeleted))
		msg += "\n"
	}

	msg += "Total snapshots that will be kept: "
	msg += strconv.Itoa((len(allSnapShots) - len(toBeDeleted)))

	SendToSlack(msg)

	// delete the to be deleted slice of snapshots
	DeleteSnapShots(toBeDeleted, &dryRunPtr)
}

// Get a list of every snapshot in our account
func GetAllSnapshots(awsAccountId string) snapShotSlice {

	// get snapshots from aws
	respDscrSnapshots, err := svc.DescribeSnapshots(&ec2.DescribeSnapshotsInput{
		OwnerIds: []*string{&awsAccountId},
	})
	CheckErr(err)

	// this print pure amazon response
	if printAllPtr {
		fmt.Println("unsorted: ", respDscrSnapshots.Snapshots)
	}

	// sort them
	sorted := SortByDate(respDscrSnapshots.Snapshots)

	return sorted

}

// sort by date
func SortByDate(snapshots []*ec2.Snapshot) snapShotSlice {
	snaps := make(snapShotSlice, 0, len(snapshots))

	for _, snap := range snapshots {
		s := EBS{
			Description: *snap.Description,
			SnapshotId:  *snap.SnapshotId,
			StartTime:   *snap.StartTime,
		}
		snaps = append(snaps, s)
	}

	sort.Sort(snaps)
	return snaps
}

// seperate and return older than a year and less than year old snapshots
func YearSplit(snapshots snapShotSlice) (snapShotSlice, snapShotSlice) {

	var t time.Time
	var yearOld snapShotSlice
	var yearYoung snapShotSlice
	aYearAgo := now.AddDate(-1, 0, 0)

	for _, snapshot := range snapshots {
		t = snapshot.StartTime
		if aYearAgo.After(t) {
			yearOld = append(yearOld, snapshot)
		} else {
			yearYoung = append(yearYoung, snapshot)
		}
	}

	return yearOld, yearYoung
}

// older than what was passed by older-than flag for passed slice group
func OlderThan(olderThan string, snaps snapShotSlice) snapShotSlice {
	var older snapShotSlice

	olderParsedToHours, err := ParseOlderThanToHours(olderThan)
	CheckErr(err)

	for _, snap := range snaps {
		duration := now.Sub(snap.StartTime)
		if duration.Hours() > olderParsedToHours {
			if verbosePtr {
				fmt.Printf("%s %s Is older than flag\n", snap.SnapshotId, snap.StartTime)
			}
			older = append(older, snap)
		} else {
			if verbosePtr {
				fmt.Printf("%s %s Is not older than flag\n", snap.SnapshotId, snap.StartTime)
			}
		}
	}

	return older
}

// return same group of snapshots without the first snapshot of the month
func RemoveFirstOfMonth(snaps snapShotSlice) snapShotSlice {
	var withoutFirstOfMonth snapShotSlice
	var firstOfMonth snapShotSlice
	months := map[time.Month]EBS{}

	for _, snap := range snaps {
		month := snap.StartTime.Month()
		if _, ok := months[month]; !ok {
			months[month] = EBS{
				Description: snap.Description,
				SnapshotId:  snap.SnapshotId,
				StartTime:   snap.StartTime,
			}
			firstOfMonth = append(firstOfMonth, snap)
			if verbosePtr {
				fmt.Printf("%s %s Not in array, must be fist of month, do nothing\n", snap.SnapshotId, snap.StartTime)
			}
		} else {
			months[month] = EBS{
				Description: snap.Description,
				SnapshotId:  snap.SnapshotId,
				StartTime:   snap.StartTime,
			}
			withoutFirstOfMonth = append(withoutFirstOfMonth, snap)
			if verbosePtr {
				fmt.Printf("%s %s Must already have a snap shot in array\n", snap.SnapshotId, snap.StartTime)
			}
		}
	}

	return withoutFirstOfMonth
}

// Parse old than flag to hours so that we can use the value for deletion step
func ParseOlderThanToHours(olderThan string) (float64, error) {
	var minutes float64
	var hours float64

	// Parse our date range
	match, _ := regexp.MatchString("^[0-9]*(h|d|m)$", olderThan)
	if !match {
		return hours, errors.New("The --older-than value of \"" + olderThan + "\" is not formatted properly.  Use formats like 30d or 24h")
	}

	// We were given a time like "12h"
	if match, _ := regexp.MatchString("^[0-9]*(h)$", olderThan); match {
		hours, _ = strconv.ParseFloat(olderThan[0:len(olderThan)-1], 64)
	}

	// We were given a time like "15d"
	if match, _ := regexp.MatchString("^[0-9]*(d)$", olderThan); match {
		hours, _ = strconv.ParseFloat(olderThan[0:len(olderThan)-1], 64)
		hours *= 24
	}

	// We were given a time like "5m"
	if match, _ := regexp.MatchString("^[0-9]*(m)$", olderThan); match {
		minutes, _ = strconv.ParseFloat(olderThan[0:len(olderThan)-1], 64)
		hours = minutes / 60
	}

	return hours, nil
}

// concatinate two arrays and return one
func ConcatSlices(olderThan, yearOld snapShotSlice) snapShotSlice {
	var combined snapShotSlice
	combined = append(olderThan, yearOld...)
	return combined
}

// Print each individual snapshot id plus total at the end
func printList(list []*ec2.Snapshot) {
	for _, snp := range list {
		fmt.Println(snp)
	}
	fmt.Println("Total number of snapshots:", len(list))
}

// delete snapshots passed
func DeleteSnapShots(snapshots snapShotSlice, dryRun *bool) {
	for _, snap := range snapshots {

		// make the delete call
		_, err := svc.DeleteSnapshot(&ec2.DeleteSnapshotInput{
			DryRun:     dryRun,
			SnapshotId: &snap.SnapshotId,
		})

		// handle the output from the delete call
		if err != nil { // if an error has occured

			// if error response container in use
			if strings.ContainsAny(string(err.Error()), "InvalidSnapshot & InUse") {
				msg := "Snapshot in use: "
				msg += snap.SnapshotId
				msg += "\n"
				fmt.Println("Error:", err.Error())
				// SendToSlack(msg)
			} else { // all other error types
				fmt.Println("Some other error has occured:", err.Error())
				fmt.Println("Error:", err.Error())
				msg := "Some other error has occured with snapshot: "
				msg += snap.SnapshotId
				msg += "\n"
				// SendToSlack(msg)
			}
		} else { // successful deletion
			fmt.Println("Snapshot is getting deleted:", snap.SnapshotId)
			msg := "Snapshot is getting deleted: "
			msg += snap.SnapshotId
			msg += "\n"
			// SendToSlack(msg)
		}
	}
}

// Check errors
func CheckErr(err error) {
	if err != nil {
		panic(err)
	}
}

// output to slack channel
func SendToSlack(msg string) {
	channel := "#devops-reporting"
	username := "jenkins"
	url := "https://hooks.slack.com/services/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"

	json := []byte(`{"channel":"` + channel + `","username":"` + username + `","text":"` + msg + `"}`)

	resp, err := http.Post(url, "application/json", bytes.NewBuffer(json))
	CheckErr(err)

	defer resp.Body.Close()
}

// Print Usage
func Usage() {
	fmt.Printf("Usage of %s:\n", os.Args[0])
	fmt.Printf("     Delete AWS EBS Volumes\n")
	flag.PrintDefaults()
}

// Print Version
func Version() {
	fmt.Println(AppVersion)
	os.Exit(0)
}
